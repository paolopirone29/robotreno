﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
    direzioni maschera di bit 
    
        giu su destra sinistra  |  intero   
         0  0    0      0            0
         0  0    0      1            1
         0  0    1      0            2
         0  0    1      1            3
         0  1    0      0            4
         0  1    0      1            5
         0  1    1      0            6
         0  1    1      1            7
         1  0    0      0            8
         1  0    0      1            9
         1  0    1      0            10
         1  0    1      1            11
         1  1    0      0            12
         1  1    0      1            13
         1  1    1      0            14
         1  1    1      1            15

         intero 1,2,4,8 sono le posizioni particolari alle estremità
         intero 7,11,13,14,15 sono le posizioni regolate dagli scambi
         intero 3 avanti dietro, intero 12 sopra sotto.
        
         primi 4 bit per la direzione, il bit 5 per indicare se un binario è una STAZIONE numeri da 16 a 31
         settiamo solo i binario non scambio come stazione  
    
         6 bit per binario ROTTO! numeri da 32 a 63.  
     
     */

public class BinarioScript {

    public Vector2 indice;
    public GameObject gameObject;
    public Vector3 posizione_arena;
    public int direzioni_possibili;
    public bool has_scambio;
    public int direzione_scambio;
    public bool isStation;
    public int pointStation;
    public Material material;
    public bool is_rotto;

    public BinarioScript(Vector2 vector2, GameObject temp, Vector3 pos, int v, bool has_scambio, bool isStation, bool is_rotto)
    {
        this.indice = vector2;
        this.gameObject = temp;
        this.posizione_arena = pos;
        this.direzioni_possibili = v & 15;
        this.has_scambio = has_scambio;
        this.isStation = isStation;
        this.material = temp.GetComponent<MeshRenderer>().material;
        this.is_rotto = is_rotto;
        int result = 0;
        int value = 0;
        if (has_scambio)
        {
            do
            {
                int apice = Mathf.RoundToInt(Random.Range(0, 3));
                value = (int)Mathf.Pow(2, apice);
                result = value & this.direzioni_possibili;
               
            }
            while (result == 0);
        }
        else if (isStation)
        {
            this.pointStation = Mathf.RoundToInt(Random.Range(1, 10));
           
        }
        else if (Mathf.IsPowerOfTwo(direzioni_possibili))
        {
            value = direzioni_possibili;
            this.gameObject.layer = 0;
        }
        

        direzione_scambio = value;
        if (direzione_scambio == 1) this.gameObject.transform.rotation = Quaternion.Euler(0, 180, 0);
        else if (direzione_scambio == 4) this.gameObject.transform.rotation = Quaternion.Euler(0, 270, 0);
        else if (direzione_scambio == 8) this.gameObject.transform.rotation = Quaternion.Euler(0, 90, 0);

    }

    public void Change_scambio()
    {
        int result = 0;
        int value = 0;
        do
        {
            value = this.direzione_scambio * 2;
            value = value > 8 ? 1 : value;
            result = value & this.direzioni_possibili;
            this.direzione_scambio = value;
        }
        while (result == 0);

        this.direzione_scambio = value;
        if (direzione_scambio == 1) this.gameObject.transform.rotation = Quaternion.Euler(0, 180, 0);
        else if (direzione_scambio == 2) this.gameObject.transform.rotation = Quaternion.Euler(0, 0, 0);
        else if (direzione_scambio == 4) this.gameObject.transform.rotation = Quaternion.Euler(0, 270, 0);
        else if (direzione_scambio == 8) this.gameObject.transform.rotation = Quaternion.Euler(0, 90, 0);
    }

    public void SetScambio(int value) {

        this.direzione_scambio = value;
        if (direzione_scambio == 1) this.gameObject.transform.rotation = Quaternion.Euler(0, 180, 0);
        else if (direzione_scambio == 2) this.gameObject.transform.rotation = Quaternion.Euler(0, 0, 0);
        else if (direzione_scambio == 4) this.gameObject.transform.rotation = Quaternion.Euler(0, 270, 0);
        else if (direzione_scambio == 8) this.gameObject.transform.rotation = Quaternion.Euler(0, 90, 0);
    }

    public void BaseMat() {

        this.gameObject.GetComponent<MeshRenderer>().material = this.material;
    }

    public void ChangeMat(Material mat) {
        this.gameObject.GetComponent<MeshRenderer>().material = mat;
    }

    public void Aggiusta() {
        this.is_rotto = false;
        this.ChangeMat(this.material);
    }
        
}





