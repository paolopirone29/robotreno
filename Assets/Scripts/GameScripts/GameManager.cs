﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public Vector2 area = new Vector2(3,1.8f);
    public float larghezza_binario = 0.3f;
    //topografia binari
    int[,] matrix = new int[6, 10]{/*
                                   {0 ,10,35,19,9 ,0 ,10,35,9 ,1 },
                                   {0 ,12,0 ,0 ,12,0 ,12,0 ,12,0 },
                                   {0 ,22,3 ,3 ,15,3 ,7 ,3 ,13,1 },
                                   {0 ,0 ,0 ,0 ,12,0 ,0 ,0 ,12,0 },
                                   {0 ,18,3 ,3 ,7 ,3 ,35,3 ,21,0 },
                                   {0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 }
                                   */
                                   {0 ,0 ,0 ,0 ,0 ,0 ,24,0 ,0 ,0 },
                                   {0 ,18,35,11,3 ,3 ,15,3 ,3 ,17},
                                   {0 ,0 ,0 ,44,0 ,0 ,12,0 ,0 ,0 },
                                   {0 ,0 ,0 ,12,0 ,0 ,12,0 ,0 ,0 },
                                   {0 ,18,35,7 ,3 ,35,7 ,3 ,3 ,1 },
                                   {0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 }
                                    };
                                  
    public Dictionary<Vector2, BinarioScript> binari = new Dictionary<Vector2, BinarioScript>();
    public List<Vector2> ordineStazioni = new List<Vector2>();
    int next_station = 0;
    public int numero_fermate = 4;
    public int lose_point = 1;
    int lose_tot = 0;
    public bool game_finish;
    public bool game_start;
    public Material fermata_mat;
    public Material fermata_mat_ill;
    public Material rotto_mat;
    public GameObject prefab_binario;
    public GameObject prefab_binario_scambio;
    public GameObject prefab_binario_onedir;
    public GameObject prefab_binario_station;
    //intero 1,2,4,8 direzioni sx dx su giu
    public int direction = 1;//sx
    public Vector2 start = new Vector2(3,9); //indice matrice di start
    public Vector2 current;

    public GameObject player;
    public int punti_player = 5;

    float next;
    
    public Text punteggio_text;
    //public Text punti_text;

    //pid variables
    [Range(0f, 100f)] public float kp, ki, kd, beta, N, tollerance, saturation;
    [Range(0f, 100f)] public float kp1, ki1, kd1, beta1, N1, tollerance1;
    [Range(1f, 100f)] public int minVel, maxAngularVel;
    [Range(0.5f, 100f)] public float delayPID;
    private JSController jsc;
    public GameObject robot;
    private Vector3 target;
    public GameObject user;
    public float delayUser;
    float timeUser;
    Vector2 old_user_pos = new Vector2(-1,-1);

    BinarioScript scambio_lock;
    AudioSource audio_source;
    public AudioClip right_station;
    public AudioClip block_sound;
    public AudioClip error_sound;
    public AudioClip start_sound;
    public AudioClip vittoria_sound;
    public AudioClip sconfitta_sound;
    public AudioClip sound_aggiusta;

    public float timervalue;
    public float timer;
    public float bloccato_time;
    public float bloccato_max_time = 3.0f;
    //public float timer_aggiusta = 1.0f;
    //anim stazione
    private bool round = false;

    BinarioScript rotto_lock;
    float timer_rotto;
    public float timer_rotto_max = 1.0f;

    bool first = true;

    // Use this for initialization
    void Start () {
        audio_source = GetComponent<AudioSource>();
        LoadScene();
    }

    void LoadScene()
    {
        float x = (-area.x / 2) + (larghezza_binario / 2);
        float z = (area.y / 2) - (larghezza_binario / 2);
        Vector3 pos = new Vector3(x, 0.0f, z);
       
        for (int i = 0; i < 6; i++)
        {
            pos.x = x;
            for (int j = 0; j < 10; j++)
            {
                if (matrix[i, j] != 0)
                {
                    bool has_scambio = false;
                    bool is_station = false;
                    bool is_rotto = false;
                    GameObject temp;
                    if (matrix[i, j] == 7 || matrix[i, j] == 11 || matrix[i, j] >= 13 && matrix[i, j] < 16)
                    {
                        has_scambio = true;
                        temp = GameObject.Instantiate(prefab_binario_scambio, GameObject.Find("Environment").transform);
                    }
                    else if ((matrix[i, j] & 16) != 0)
                    {
                        //Debug.Log(matrix[i, j]);
                        is_station = true;
                        GameObject prefab = prefab_binario_station;
                        temp = GameObject.Instantiate(prefab, GameObject.Find("Environment").transform);
                        ordineStazioni.Add(new Vector2(i, j));
                    }
                    else if (Mathf.IsPowerOfTwo(matrix[i, j]))
                    {
                        temp = GameObject.Instantiate(prefab_binario_onedir, GameObject.Find("Environment").transform);
                    }
                    else
                    {
                        temp = GameObject.Instantiate(prefab_binario, GameObject.Find("Environment").transform);
                        if ((matrix[i, j] & 32) != 0)
                            is_rotto = true;
                    }
                    temp.transform.position = pos;
                    binari.Add(new Vector2(i, j), new BinarioScript(new Vector2(i, j), temp, pos, matrix[i, j], has_scambio, is_station, is_rotto));
                    if (is_rotto)
                        binari[new Vector2(i, j)].ChangeMat(rotto_mat);
                }
                pos.x = pos.x + larghezza_binario;
            }

            pos.z = pos.z - larghezza_binario;
        }

        current = start;
        player.transform.position = binari[current].posizione_arena;
        next = Time.time;
        ShuffleStazioni(numero_fermate);
        game_finish = false;
        game_start = false;
        SetStazione();

        timeUser = Time.time;

        jsc = robot.transform.GetChild(2).gameObject.GetComponent<JSControllerContinous>();
        //jsc = robot.transform.GetChild(1).gameObject.GetComponent<JSControllerDiscrete>();
        jsc.SetUp(kp, ki, kd, beta, (int)N, tollerance, saturation, minVel, maxAngularVel,
            kp1, ki1, kd1, beta1, (int)N1, tollerance1, delayPID);
        jsc.OnTargetReached += HandleTargetReached;
        jsc.forcemin = true;

        target = binari[current].posizione_arena;
        timer = timervalue;
        punteggio_text.text = punti_player.ToString();
    }

    void ShuffleStazioni(int numero) {
        Vector2[] temp = new Vector2[ordineStazioni.Count];
        ordineStazioni.CopyTo(temp);
        ordineStazioni.Clear();

        Vector2 old = new Vector2(-1, -1);
        Vector2 nuova;
        for (int i = 0; i < numero;) {
            int k = Mathf.RoundToInt(Random.Range(0, temp.Length));
            nuova = temp[k];
            if (nuova != old)
            {
                ordineStazioni.Add(temp[k]);
                i++;
            }
            old = nuova;
        }
    }

    void HandleTargetReached()
    {
        jsc.ResetPids(true, true);
        BinarioScript binario = binari[current];
        int dir = 0;
        if (!binario.has_scambio)
        {
            if (Mathf.IsPowerOfTwo(binario.direzioni_possibili))
                dir = binario.direzioni_possibili;
            else if ((binario.direzioni_possibili & direction) == 0)
            {
                int mask = 0;
                if (direction == 4 || direction == 8) mask = 3;
                else mask = 12;
                dir = binario.direzioni_possibili & mask;
            }
            else
                dir = direction;
        }
        else
        {
            dir = binario.direzione_scambio;
        }
        if (binari[current].isStation || binari[current].is_rotto)
        {
            AggiornaUI(binari[current]);
        }
        current.x = current.x + ((dir & 12) == 4 ? -1 : (dir & 12) == 8 ? +1 : 0);
        current.y = current.y + ((dir & 3) == 1 ? -1 : (dir & 3) == 2 ? +1 : 0);

        target = binari[current].posizione_arena;
        direction = dir;
    }

    // Update is called once per frame
    void Update () {

        if (!game_finish && game_start)
        {

            if (Time.time > next)
            {
                AnimaStazione(binari[ordineStazioni[next_station]].gameObject);

                try
                {
                    Vector3 user_pos = user.transform.position;
                    if (Mathf.Abs(target.x - user_pos.x) <= 0.30 && Mathf.Abs(target.z - user_pos.z) <= 0.30)
                    {
                        Debug.Log("bloccato");
                        
                        bloccato_time -= delayPID;
                        if (bloccato_time <= 0)
                        {
                            audio_source.clip = block_sound;
                            audio_source.Play();
                            punti_player -= lose_point;
                            if (punti_player <= 0)
                                game_finish = true;
                            bloccato_time = bloccato_max_time;
                        }
                        punteggio_text.text = punti_player.ToString();
                        jsc.SetControllMovement(false);
                    }
                    else
                    {
                        jsc.SetTarget(target);
                        jsc.SetControllMovement(true);
                    }
                }
                finally
                {
                    next = Time.time + delayPID;

                }

                timer -= next - Time.time;
                if (scambio_lock != null) {
                    if (timer > 0)
                        StartCoroutine(LongVibration(0.1f,200));
                }
            }

            {
                Vector3 posizione = user.GetComponent<KalmanScript>().position;
                float x = posizione.x;
                float z = posizione.z;

                Vector2 indice = new Vector2();

                indice.x = Mathf.Abs((int)(((z / area.y) * (area.y / larghezza_binario)) - (area.y / larghezza_binario / 2)));
                indice.y = (int)(((x / area.x) * (area.x / larghezza_binario)) + (area.x / larghezza_binario / 2));

                if (binari.ContainsKey(indice))
                {
                    BinarioScript binario = binari[indice];
                    if (binario.has_scambio)
                    {
                       scambio_lock = binario;
                    }
                    else if (binario.is_rotto)
                    {
                        if (binario != rotto_lock)
                        {
                            rotto_lock = binario;
                            timer_rotto = timer_rotto_max;
                        }
                        else {
                            timer_rotto -= Time.deltaTime;
                            if(timer_rotto <= 0)
                            {
                                audio_source.clip = sound_aggiusta;
                                audio_source.Play();
                                binario.Aggiusta();
                                timer_rotto = timer_rotto_max;
                            }
                        }
                        
                    }
                    else
                        scambio_lock = null;

                    if (old_user_pos != indice)
                        timer = timervalue;

                    old_user_pos = indice;
                   
                }

            }

            if (scambio_lock != null && timer <= 0){
                Vector3 s = -user.transform.up;
                s.y = 0;
                float yAngle = Mathf.Atan2(s.x, s.z) * Mathf.Rad2Deg;
               
                int dir = 0;
                if (yAngle > 45 && yAngle <= 135) dir = 2;
                else if ((yAngle > 135 && yAngle <= 180) || (yAngle >= -180 && yAngle < -135)) dir = 8;
                else if (yAngle >= -135 && yAngle <= -45) dir = 1;
                else if ((yAngle > -45 && yAngle <= 0) || (yAngle >= 0 && yAngle < 45)) dir = 4;

                if ((scambio_lock.direzioni_possibili & dir) != 0)
                    scambio_lock.SetScambio(dir);

                timer = timervalue;
                StartCoroutine(LongVibration(0.3f, 500));
                scambio_lock = null;

            }

        }
        else if(game_finish)
        {
            jsc.SetControllMovement(false);
            jsc.ResetPids(true, true);
            punteggio_text.text = punti_player.ToString();
            if (punti_player > 0)
            {
                audio_source.clip = vittoria_sound;
                audio_source.Play();
            }
            else {
                audio_source.clip = sconfitta_sound;
                audio_source.Play();
            }

            Transform t = GameObject.Find("Environment").transform;
            for (int i = 1; i < t.childCount; i++)
            {
                GameObject.Destroy(t.GetChild(i).gameObject);
            }
            next_station = 0;
            binari.Clear();
           
            

        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if(!first)
                LoadScene();
            first = false;
            game_start = !game_start;
            user.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;
            robot.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;
            audio_source.clip = start_sound;
            audio_source.Play();
        }
        
    }

    void AggiornaUI(BinarioScript binario) {
        if (binario.is_rotto)
        {
            punti_player = 0;
            game_finish = true;
        }
        else if (binario.indice == ordineStazioni[next_station])
        {
            audio_source.clip = right_station;
            audio_source.Play();
            punti_player += lose_point;
            next_station++;
            if (next_station >= ordineStazioni.Count)
            {

               game_finish = true;
            }
            else
            {
                SetStazione();
                BinarioScript b;
                do
                {
                    int row = Random.Range(0, 5);
                    int col = Random.Range(0, 9);
                    if (binari.ContainsKey(new Vector2(row, col)))
                        b = binari[new Vector2(row, col)];
                    else
                        b = null;
                }
                while (b == null || b.isStation || b.has_scambio);

                b.is_rotto = true;
                b.ChangeMat(rotto_mat);
            }
        }
        else
        {
            audio_source.clip = error_sound;
            audio_source.Play();
            punti_player -= lose_point;
            lose_tot += lose_point;
            if (punti_player < 0)
            {
                game_finish = true;
               
            }
        }

        
        punteggio_text.text = punti_player.ToString();
    }

    void SetStazione()
    {
        BinarioScript old;
        BinarioScript nuova;
        if (next_station > 0)
        {
            old = binari[ordineStazioni[next_station - 1]];
            old.BaseMat();
        }
        
        nuova = binari[ordineStazioni[next_station]];
        nuova.ChangeMat(fermata_mat);

    }

    IEnumerator LongVibration(float length, float strength)
    {
        for (float i = 0; i < length; i += Time.deltaTime)
        {
            SteamVR_Controller.Input((int)user.GetComponent<SteamVR_TrackedObject>().index)
                .TriggerHapticPulse((ushort)Mathf.Lerp(0, 3999, strength));

            yield return null;
        }
    }

    IEnumerator PulseVibration(int vibrationCount, float vibrationLength, float gapLength, float strength)
    {
        strength = Mathf.Clamp01(strength);
        for (int i = 0; i < vibrationCount; i++)
        {
            if (i != 0) yield return new WaitForSeconds(gapLength);
            yield return StartCoroutine(LongVibration(vibrationLength, strength));
        }
    }

    void AnimaStazione(GameObject gameObject) {

        if (!round)
            gameObject.GetComponent<MeshRenderer>().material = fermata_mat_ill;
        else
            gameObject.GetComponent<MeshRenderer>().material = fermata_mat;

        round = !round;
    }
}
