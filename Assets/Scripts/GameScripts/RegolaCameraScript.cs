﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegolaCameraScript : MonoBehaviour {

    public Vector3 postion;
    public Vector3 angles;
    public float size;
    public GameObject cameravr;

    // Use this for initialization
    void Start()
    {
        cameravr.SetActive(false);
        this.GetComponent<Camera>().orthographic = true;
        this.GetComponent<Camera>().stereoTargetEye = StereoTargetEyeMask.None;
    }

	// Update is called once per frame
	void LateUpdate () {

        this.transform.position = postion;
        this.transform.rotation = Quaternion.Euler(angles);
        if (Input.GetKeyDown(KeyCode.Q))
            size += 0.01f;
        else if (Input.GetKeyDown(KeyCode.E))
            size -= 0.01f;

        if (Input.GetKeyDown(KeyCode.A))
            postion.x += 0.01f;
        else if (Input.GetKeyDown(KeyCode.D))
            postion.x -= 0.01f;

        if (Input.GetKeyDown(KeyCode.W))
            postion.z -= 0.01f;
        else if (Input.GetKeyDown(KeyCode.S))
            postion.z += 0.01f;

        this.GetComponent<Camera>().orthographicSize = size;
    }
}
