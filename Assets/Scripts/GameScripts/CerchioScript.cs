﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CerchioScript : MonoBehaviour {

    public GameObject target;
    public float y;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        Vector3 pos = target.transform.position;
        pos.y = y;
        transform.position = pos;
	}
}
