﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

[Serializable]
class JSCommand {
        
	private string type;
    private int[] parameters;

    public JSCommand(string type, int[] parameters) {
	
		this.type = type;
        this.parameters = parameters;
    }

    public string Type { get { return type; } set { type = value; } }
    public int[] Parameters { get { return parameters; } set { parameters = value; } }
    
}
