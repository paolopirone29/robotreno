﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using System;

public class JSCommandSender {

	public const string MOVE_COMMAND = "move";
    public const string VOLUME_COMMAND = "volume";
    public const string ANIM_COMMAND = "anim";
    public const string BATTERY_COMMAND = "battery";

    private ClientSocketUDP socket;
	private string ip_server;
	private int port_server;

	public JSCommandSender(string ip_server, int port_server){
	
		socket = new ClientSocketUDP ();
		this.ip_server = ip_server;
		this.port_server = port_server;
	}

	public void Forward(int speed){
	
		JSCommand command = new JSCommand (MOVE_COMMAND, new int[]{ speed, 0, 0 });
		string json = JsonConvert.SerializeObject (command);
		socket.Send (json,ip_server,port_server);
	}

    public void Turn(int degree) {

        JSCommand command = new JSCommand(MOVE_COMMAND, new int[] { 0, degree, 0 });
        string json = JsonConvert.SerializeObject(command);
        socket.Send(json, ip_server, port_server);
    }

    public void Move(int speed, int turn){
        JSCommand command = new JSCommand(MOVE_COMMAND, new int[] { speed,turn, 0 });
        string json = JsonConvert.SerializeObject(command);
        socket.Send(json, ip_server, port_server);
    }

    public void Volume(int volume) {
        JSCommand command = new JSCommand(VOLUME_COMMAND, new int[] {volume,0, 0 });
        string json = JsonConvert.SerializeObject(command);
        socket.Send(json, ip_server, port_server);
    }

    public void Animation(string animation) {
        int anim = -1;
        animation = animation.ToLower();
        if (animation == "stop") anim = 0;
        else if (animation == "metronome") anim = 1;
        else if (animation == "ondulation") anim = 2;
        else if (animation == "slalom") anim = 3;
        else if (animation == "slowshake") anim = 4;
        else if (animation == "spin") anim = 5;
        else if (animation == "spinjump") anim = 6;
        else if (animation == "spintoposture") anim = 7;
        else if (animation == "spiral") anim = 8;
        else if (animation == "tap") anim = 9;

        if (anim > -1) {
            JSCommand command = new JSCommand(ANIM_COMMAND, new int[] {anim,0,0});
            string json = JsonConvert.SerializeObject(command);
            socket.Send(json, ip_server, port_server);
        }
    }

    public void AddBatteryListener(string this_ip, int this_port) {
        string[] split = this_ip.Split('.');
        int[] param = new int[split.Length+1];
        for (int i = 0; i < split.Length; i++) {
            param[i] = int.Parse(split[i]);
        }
        param[split.Length] = this_port;
        JSCommand command = new JSCommand(BATTERY_COMMAND,param);
        string json = JsonConvert.SerializeObject(command);
        socket.Send(json, ip_server, port_server);
    }

}
