﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net.Sockets;
using System.Net;
using System.Text;

public class ClientSocketUDP {

	private Socket clientSocket;

	public ClientSocketUDP(){
		clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
	}

	public void Send(string toSend, string ip_server,int port){
	
		IPAddress server_ip = IPAddress.Parse(ip_server);

		byte[] sendbuf = Encoding.ASCII.GetBytes(toSend);
		IPEndPoint ep = new IPEndPoint(server_ip,port);

		clientSocket.SendTo(sendbuf, ep);

		//Debug.Log("Message sent to server: "+toSend);
	}
}
