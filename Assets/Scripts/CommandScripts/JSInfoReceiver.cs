﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

public class JSInfoReceiver {

    private UdpClient listener;
    private string ip_server;
    private int port_server;

    public JSInfoReceiver(int port_server) {
        this.port_server = port_server;
        listener = new UdpClient(port_server);
    }

    public int Receive() {
        IPEndPoint groupEP = new IPEndPoint(IPAddress.Any, port_server);
        byte[] receivedBytes = listener.Receive(ref groupEP);
        Debug.Log(receivedBytes[0]);
        return receivedBytes[0];
    }

}
