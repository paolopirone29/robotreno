﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Accord.Statistics.Running;

public class KalmanScript : MonoBehaviour {

    public Vector3 position;
    private KalmanFilter2D kalman;

    
	// Use this for initialization
	void Start () {
        kalman = new KalmanFilter2D(30, 1, 1);
	}
	
	// Update is called once per frame
	void Update () {

        Vector3 track_pos = this.transform.position;
        kalman.Push(track_pos.x, track_pos.z);
        position = new Vector3((float)kalman.X, track_pos.y, (float)kalman.Y);
	}
}
