﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JSControllerContinous : JSController {

    protected override void ControllingLogicMovement()
    {
        //curr_position = this.transform.position;

        float turn = 0;
        float vel = 0;

        base.ControllingTurn(ref turn);
        base.ControllingSpeed(ref vel);

        if (Mathf.Abs(turn) > tollerance1)
            vel = vel > maxAngularVel ? maxAngularVel : vel;

        base.velocity = Mathf.RoundToInt(vel);
        base.turn = Mathf.RoundToInt(turn);
    }
}
