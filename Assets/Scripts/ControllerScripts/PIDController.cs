﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PIDController {

	private float kp;
	private float kd;
	private float ki;
	private float beta; //peso del riferimento
	private float N_order;

	private float old_inputI = 0.0f;
	private float old_inputD = 0.0f;
	private float old_proc_var = 0.0f;
	private float kt;
	private float ti;
	private float td;
	private float tf;
    private int sign = 1;
	private bool saturated = false;

	public PIDController(float kp, float kd, float ki, float beta,float N_order){
	
		this.kp = kp;
		this.kd = kd;
		this.ki = ki;
		this.beta = beta;
		this.N_order = N_order;

		ti = kp / ki;
		td = kd / kp;
		tf = kd/N_order;

		kt = Mathf.Sqrt (ti * td);

	}

    public bool Controller
        (float reference, float proc_var, float currTime, float oldTime, float tollerance, float maxSat, float minSat, out float value)
    {

        float error = (reference - proc_var);
        //Debug.Log("Errore :" + error);

        if ( Mathf.Abs(error) - tollerance > 0)
        {

            float h = currTime - oldTime;

            float inputP = kp * (beta * reference - proc_var);
            //Debug.Log("Prop :" + inputP);

            float inputI = 0;
            if (!saturated)
                inputI = old_inputI + ki * error * h; //eliminando i valori integrali passati si ottiene un migliore controllo
            else
                inputI += sign * old_inputI;
            //Debug.Log("Integrale :" + inputI);

            float diff = proc_var - old_proc_var == 0  || old_proc_var == 0 ? Mathf.Abs(error) : proc_var - old_proc_var;
            float inputD = old_inputD * tf / (tf + h) - kd * (diff) / (tf + h);
            old_proc_var = proc_var;
            old_inputD = inputD;
            //Debug.Log("derivata :" + inputD);

            float input = inputP + inputI + inputD;

            if (input >= maxSat || input <= minSat)
            {

                float saturation = 0;
                float newInput = 0;

                if (input > maxSat)
                {
                    sign = -1;
                    newInput = maxSat;
                    saturation = maxSat;
                }
                else if (input < minSat)
                {
                    sign = +1;
                    newInput = minSat;
                    saturation = minSat;
                }


                inputI = sign * h * kt * (saturation - input);

                input = newInput;

                saturated = true;
            }
            saturated = false;

            old_inputI = inputI;

            value = input;
            return false;
        }
        else
        {
            value = 0;
            return true;
        }
    }

    public void Reset() {

        old_inputI = 0.0f;
        old_inputD = 0.0f;
        old_proc_var = 0.0f;
        sign = 1;
        saturated = false;
    }
}
