﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
//using JumpingParrot.Drone.Manager;
using System.Threading;

public class JSController : MonoBehaviour
{

    public bool forcemin = false;
    public bool scatta;

    //js socket
    private JSCommandSender sender;
    //private DroneManager drone;
    string address_js = "127.0.0.1";
    int port_js = 1200;

    //js setting
    protected int minVel;
    protected int maxAngularVel;
    protected int velocity = 0;
    protected int turn = 0;

    //pid
    private PIDController pid_distance;
    private PIDController pid_turn;
    private float distance_ref;
    private float angle_ref;
    private float currTime, prevTime;

    //pid variable
    protected float kp, ki, kd, beta, N, tollerance, saturation;
    protected float kp1, ki1, kd1, beta1, N1, tollerance1;

    //frquenza 
    float delay = .5f;
    float next_time = 0.0f;

    //target
    protected Vector3 target;
    protected Vector3 prev_target;
    protected Vector3 curr_position;

    //delegate events
    public delegate void TargetReached();
    public event TargetReached OnTargetReached;

    //bool vars
    bool isSet = false;
    bool isControllingMovement = false;
    

    // Use this for initialization
    public void SetUp(float kp, float ki, float kd, float beta, int N, float tollerance, float saturation,
       int minVel, int maxAngularVel,
       float kp1, float ki1, float kd1, float beta1, int N1, float tollerance1,
       float delay)
    {

        this.tollerance = tollerance;
        this.tollerance1 = tollerance1;
        this.saturation = saturation;
        this.minVel = minVel;
        this.maxAngularVel = maxAngularVel;
        this.delay = delay;
        //start communication server js or lib js
        sender = new JSCommandSender(address_js, port_js);
        sender.AddBatteryListener("127.0.0.1", 1400);
        sender.Volume(0);
        //drone = new DroneManager(3456, DroneManager.JUMPING_SUMO, "Paul");
        pid_distance = new PIDController(kp, kd, ki, beta, N);
        pid_turn = new PIDController(kp1, kd1, ki1, beta1, N1);
        isSet = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (isSet)
        {
            ControllMovement();
        }

        if (scatta)
        {
            if (OnTargetReached != null)
                OnTargetReached();
            scatta = false;
        }
    }

    void ControllMovement() {
        if (isControllingMovement && Time.time > next_time) {
            prevTime = currTime;
            currTime = Time.time;

            curr_position = this.transform.parent.GetComponent<KalmanScript>().position;
            curr_position.y = 0;
            //virtual method
            ControllingLogicMovement();
            
            //send to js server command or droneLib
            sender.Move(velocity, turn);
            //drone.Move((sbyte)velocity,(float) turn);

            //check if js is around the target position
            //in that case js stops
            if (Mathf.Abs(target.x - curr_position.x) <= tollerance &&
                Mathf.Abs(target.z - curr_position.z) <= tollerance)
            {
               
                if (OnTargetReached != null)
                    OnTargetReached();
            }

            //update next time
            next_time = Time.time + delay;
        }
    }

    protected virtual void ControllingLogicMovement()
    {
        
    }

    protected bool ControllingTurn(ref float turn) {
        //calc angle ref, angle proc
        target.y = 0;
        curr_position.y = 0;
        Quaternion refeQ = Quaternion.LookRotation(target - curr_position, Vector3.up);
        angle_ref = refeQ.eulerAngles.y;
        
        float proc_var_turn = this.transform.rotation.eulerAngles.y;
        //angolo tracker sfasato di 180 gradi
        proc_var_turn = proc_var_turn >= 180 ? proc_var_turn - 180 : proc_var_turn + 180;

        //calc min angle between ref and proc
        float minAngle = angle_ref - proc_var_turn;
        minAngle = (minAngle + 180) % 360;
        if (minAngle > 0)
            minAngle -= 180;
        else if (minAngle < 0)
            minAngle += 180;

        //set max min turn pid saturation
        float max = minAngle < 0 ? 0 : minAngle;
        float min = max == 0 ? minAngle : 0;

        return pid_turn.Controller(minAngle, 0, currTime, prevTime, tollerance1, max, min, out turn);
    }

    protected bool ControllingSpeed(ref float vel) {
        //calc distance to target
        distance_ref = Mathf.Abs(Vector3.Distance(target, curr_position));
        float proc_var = 0;

        bool finishDistance =
                this.pid_distance.Controller(distance_ref, proc_var, currTime, prevTime, tollerance, saturation, 0, out vel);

        if(!forcemin)
            vel = vel < minVel && vel > 0 ? minVel : vel;
        else
            vel = vel < minVel ? minVel : vel;

        return finishDistance;
    }

    public void SetTarget(Vector3 target) {
        this.target = target;
    }

    public void SetControllMovement(bool value) {
        isControllingMovement = value;
    }

    public void ResetPids(bool distance, bool turn) {
        if (distance) pid_distance.Reset();
        if (turn) pid_turn.Reset();
    }

    public void ChangeMinVel(float speed, float speedAngular)
    {
        if(speed > -1)
            minVel = (int)speed;
        if (speedAngular > -1)
            maxAngularVel =(int) speedAngular;
    }

    //private void OnApplicationQuit()
    //{
    //    if (drone != null) {
    //        drone.CloseConnection();
    //    }

    //}
}
