﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JSControllerDiscrete : JSController {

    bool turn_pid;
    bool distance_pid;

    int count_turn;
    int count_distance;
    public int max_turn_count = 10;
    public int max_distance_count = 10;

    protected override void ControllingLogicMovement()
    {
        //if there is new target reset pids
        if (target != prev_target)
        {
            prev_target = target;
            turn_pid = true;
            distance_pid = true;
        }

        
        //curr_position = this.transform.position;

        float turn = 0;
        float vel = 0;

        if (turn_pid && count_turn < max_turn_count)
        {
            count_turn++;
            turn_pid = !base.ControllingTurn(ref turn);
        }
        else if (distance_pid)
        {
            //check future position
            float proc_var_turn = transform.parent.rotation.eulerAngles.y;
            proc_var_turn = proc_var_turn >= 180 ? proc_var_turn - 180 : proc_var_turn + 180;
            Quaternion q = Quaternion.Euler(0, proc_var_turn, 0);
            //Debug.Log("tar " + target.ToString());
            //Debug.Log("curr " + curr_position.ToString());
            float dist = Vector3.Distance(target, curr_position);
            Vector3 fut = curr_position + q * new Vector3(0, 0, dist);
            //Debug.Log("dir "+(q * new Vector3(0, 0, dist)));
            //if (Mathf.Abs(target.x - curr_position.x) > Mathf.Abs(target.z - curr_position.z))
            //{
            //    fut = curr_position + q * new Vector3(dist, 0, 0);
            //    Debug.Log("dir " + (q * new Vector3(dist, 0, 0)));
            //}
            //Debug.Log("fut " + fut.ToString());
            //if future position is not in target, adjust turn 
            if ((Mathf.Abs(target.x - fut.x) > tollerance || Mathf.Abs(target.z - fut.z) > tollerance) && count_distance >= max_distance_count)
            {
                turn_pid = true;
                ResetPids(true, true);
                count_turn = 0;
                count_distance = 0;
                Debug.Log("turn pid is true again: fut " + fut.ToString());
            }
            else
            {
                count_distance++;
                distance_pid = !base.ControllingSpeed(ref vel);
            }

           
        }

        base.velocity = (int)Mathf.Round(vel);
        base.turn = (int)Mathf.Round(turn);
    }
}
